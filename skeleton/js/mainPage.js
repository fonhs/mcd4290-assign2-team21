// Code for the main app page (Past Runs list).


function viewRun(i){
    var Index = i;
    localStorage.setItem("Index",Index) // get the acess to view the saved run
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", i);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}

var listHTML = '' // create an empty string to update html display
var runView = document.getElementById('runView')
var runContainer = new Run()
var runArray = JSON.parse(localStorage.getItem('runObj'))
if (runArray !== null){ // display general information of the saved run (name, date)
    for(let i = 0; i<runArray.length ; i++){
        
    runContainer.initialize(runArray[i])
        
    var startTime = runContainer.startTime
    var completeTime = runContainer.endTime
    var runNameView = runContainer.runName 

    listHTML += "<tr> <td onmousedown=\"viewRun("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + 'Run name:  '+ runNameView ;

    listHTML += "<div class=\"subtitle\">"+"Date: " + startTime + "</div></td></tr>";

    runView.innerHTML = listHTML;

    }
} else if (runArray == false) {// if nothing has saved
    document.getElementById('messageToCreateRun').innerHTML = 'Create a new Run!'
}

