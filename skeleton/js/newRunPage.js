// Code for the Measure Run page.

// set current to center of Monash, creat map object
let current =[145.1329004, -37.9118667];
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 18,
    center: current
}); 

// tracking location
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions:{
        enableHighAccuracy: true
    },
    trackUserLocation: true
}))

// set global variables
var watchId,watchID1, options, longitude,latitude,coordinates,GPScoords,accuracy;
document.getElementById("save").setAttribute("disabled","")
function success1(pos) {
   longitude = pos.coords.longitude;
   latitude = pos.coords.latitude;
   current = { 
       lng: longitude,
       lat: latitude
   }
   accuracy = pos.coords.accuracy;
   document.getElementById('accuracy').innerHTML = accuracy.toFixed(2)
    if (accuracy>20) { // check whether the accuracy 
    document.getElementById("randomDestination").setAttribute("disabled","")
    document.getElementById("startRun").setAttribute("disabled","")
    document.getElementById("save").setAttribute("disabled","")
    alert(`Accuracy level too high ! Current accuracy: ${accuracy}`)
    } else {
        document.getElementById("randomDestination").removeAttribute("disabled","")
        navigator.geolocation.clearWatch(watchID1)
    }
}
options = {
  enableHighAccuracy: true,
  timeout: 50000,
  maximumAge: 0
};

function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
} 

watchID1 = navigator.geolocation.watchPosition(success1,error,options) // get current location

// get a random pont
var locations =[], distance,goodToGo;
function randomDestination (){
    var zoomlevel = map.transform._zoom;//access zoom level
    var preLocations=[]//create a array that holds local scopes
    
    if(current !== null){
        //current = map.getCenter()
        console.log(current)
        preLocations.push(current)
    }
    // get a random radius and Latitude
    randomRadius =(Math.floor(Math.random()*600)+400)/1000000;
    distance = randomRadius*150/0.001;
    var randomLat = 1;
    while(randomLat > randomRadius){
        randomLat = (Math.floor(Math.random()*600)+400)/1000000;
    }
    // get random Longtitude
    var randomLong = Math.sqrt(Math.pow(randomRadius,2)-Math.pow(randomLat,2));
    //get a random coordinate 
    var newLocation=[];
    var randomArc = Math.floor(Math.random()*4);//get a random arc a random point which can be placed
    if(randomArc ==0){//from 0-90 degree
        newLocation = {lng:current.lng+randomLong, lat:current.lat+randomLat};
    } else if(randomArc ==1){//from 90-180 degree
        newLocation = {lng:current.lng-randomLong, lat:current.lat+randomLat};
    } else if(randomArc ==2){//from 180-270 degree
        newLocation = {lng:current.lng-randomLong, lat:current.lat-randomLat};
    } else{//from 270-360 degree
        newLocation = {lng:current.lng+randomLong, lat:current.lat-randomLat};
    }
    newCoords = newLocation
    preLocations.push(newLocation);
    locations.push(preLocations);//push local array into the global array   

    // mark the start location onto map
    var popup1 = new mapboxgl.Popup()
        .setLngLat(current)
        .setHTML('Start')
        .addTo(map)
        
    var marker1 = new mapboxgl.Marker()
        .setLngLat(current)
        . addTo(map)  

        // mark the end point onto map
    var popup2 = new mapboxgl.Popup() 
        .setLngLat(newLocation)
        .setHTML('End point')
        .addTo(map)
            
    var marker2 = new mapboxgl.Marker()
        .setLngLat(newLocation)
        . addTo(map)   

    // display distance of the run
    document.getElementById("destination").innerHTML = "Approximate Distance: " +(Math.round(distance*100))/100 +" m";           
    // enable start run button
    document.getElementById("startRun").removeAttribute("disabled");
  
};

//set a glabal timer and create function to stop watch
var time,totalDurationInSecond = 0;
 /*Timer fucntion
 A functional 'stop-watch' to display duration also time in hr:min:sec format
 */
function stopWatch(){   
    time = 0
	var seconds = 0, minutes = 0, hours = 0 ;
    
    var t
	function add(){
		seconds++
        totalDurationInSecond++
		if(seconds >= 60){
			minutes++;
			seconds = 0;
			if (minutes >= 60){
				hours++
                minutes = 0
			}
		}
		time = `${hours}:${minutes}:${seconds}`
        document.getElementById('time').innerHTML = 'Time: ' + time
        t = setTimeout(add,1000);
	}
    
    this.start = function () {
        t = setTimeout(add,1000);
    };
    this.stop = function () {
        clearTimeout(t);
    };
    this.reset = function () {
        time=0;
        t = null;
        seconds = 0
        minutes = 0
        hours = 0
        totalDurationInSecond = 0;
    };
}



var watch,path=[];

// start run
var remainingDis,runObj,watchId3,oldDist;
function startRun(){
if (goodToGo !== true) { //This is used for Reattempt function() if the user is close to the starting location
    watch = new stopWatch(); //watch function
    watch.start();// starting timer

    watchId = navigator.geolocation.watchPosition(success2,error, options) //update user movement and call success2 function
    var now = new Date() 
    runObj = new Run(current,newCoords,path,getTimeStamp(now),0,0,0) //New Run Object
    function success2(pos) {
        let longitude2 = pos.coords.longitude
        let latitude2 = pos.coords.latitude
        GPScoords = [longitude2,latitude2] //Get current coordinates
        if (GPScoords !== undefined) {
            remainingDis = Math.sqrt(Math.pow(GPScoords[0]-newCoords.lng,2)+Math.pow(GPScoords[1]-newCoords.lat,2))*150000 //Calculate distance betweeen user and destination
            distanceTaken = (Math.round(distance*100))/100 - remainingDis // distance travelled by user
        }
        if (remainingDis<10) { //CHeck if user is less than 10m from the target and finish the run
            navigator.geolocation.clearWatch(watchId) //Stop locating user
            watch.stop()
            runObj.timeCalculator() //Take time from stopwatch
            var stopNow = new Date() //Get end date
            runObj.endTime = getTimeStamp(stopNow)
            alert('Run is finished! You may save this run') //Alert user for finishing the run
            document.getElementById("save").removeAttribute("disabled"); //Save button is enabled
        }
    //diaplay remaining distance
    document.getElementById('remaining').innerHTML = "Remaining Distance: " + remainingDis.toFixed(2) + ' m' //Update remaining distance
    document.getElementById('distanceTaken').innerHTML = 'Distance Travelled: ' + distanceTaken.toFixed(2) + ' m' //Update distance travelled by user
    }
    //disable  button
    document.getElementById("randomDestination").setAttribute("disabled",""); //Disable create random point button
    document.getElementById("startRun").setAttribute("disabled",""); //Disable start run button
    
    }else if (goodToGo === true) { //If user is close to the starting point (Reattempt function)
        watch = new stopWatch();
        watch.start();
        watchId3 = navigator.geolocation.watchPosition(success2,error, options)
        var now = new Date()
        runObj = new Run(newCurrentReattempt,newCoordsReattempt,path,getTimeStamp(now),0,0,0)
        var oldRunObj = JSON.parse(localStorage.getItem('runObj'))[inx] //Get the data from the saved runs
        var runContainerOld = new Run()
        runContainerOld.initialize(oldRunObj) //Re-initialize the object
        
        function success2(pos) {
            let longitude3 = pos.coords.longitude
            let latitude3 = pos.coords.latitude
            GPScoordinates = [longitude3,latitude3] //Get current location of the user
            if (GPScoordinates !== undefined) {
                remainingDis = Math.sqrt(Math.pow(GPScoordinates[0]-newCoordsReattempt.lng,2)+Math.pow(GPScoordinates[1]-newCoordsReattempt.lat,2))*150000 //Calculate the remaining distance
                distanceTaken = runContainerOld.distanceCalculator() - remainingDis //Calculate distance taken
            }
            if (remainingDis<10) { //Check if user is less than 10m away and finish the run
                navigator.geolocation.clearWatch(watchId3) //Stop locating user
                watch.stop()
                runObj.timeCalculator()//Take time from stopwatch
                var stopNow = new Date()//Get end date
                runObj.endTime = getTimeStamp(stopNow)
                alert('Run is finished! You may save this run') //Alert the user that run is finished
                document.getElementById("save").removeAttribute("disabled") //Save button is enabled
            }
        //diaplay remaining distance
        document.getElementById('remaining').innerHTML = "Remaining Distance: " + remainingDis.toFixed(2) + ' m' //Update remaining distance
        document.getElementById('distanceTaken').innerHTML = 'Distance Travelled: ' + distanceTaken.toFixed(2) + ' m' //Update distance travelled by user
        }
        //disable  button
        document.getElementById("startRun").setAttribute("disabled",""); //Disable start run button
    }

}
//reset a new run to a latest location
function resetRun(){
   location.reload() //Refresh page
 }

// re-attempt checking
var watchID2,GPScoordinates,distanceClose,newCurrentReattempt,newCoordsReattempt
if(localStorage.getItem("boolean") !== null){ //If user decided to reattempt
    
    function success3(pos) { //called when watchPosition method is executed
        let longitude3 = pos.coords.longitude
        let latitude3 = pos.coords.latitude
        distanceClose = Math.round(Math.sqrt(Math.pow(longitude3-locList._startLoc.lng,2)+Math.pow(latitude3-locList._startLoc.lat,2))*15000000)/100 //Calculate the distance from user to the starting location
        document.getElementById('distanceClose').innerHTML = 'Get closer to the starting point :  ' + distanceClose.toFixed(2) + ' m away!';
        if (distanceClose < 20){ //If the user is 20m away from the starting location, the run can start
            document.getElementById("startRun").removeAttribute("disabled") //Enable start button
            goodToGo = true; //set true for start run function
            navigator.geolocation.clearWatch(watchID2) //stop locating user
            document.getElementById('distanceClose').innerHTML = '' //Remove message
            alert('Run is good to go!')  //Alert the user that the run is good to go
            }
    }
    //disabled regenating any new points
    document.getElementById("randomDestination").setAttribute("disabled","") //Disable random point generation
    //show the saved location
    var inx = localStorage.getItem('Index');
    var locList = JSON.parse(localStorage.getItem('runObj'))[inx];
    newCurrentReattempt = locList._startLoc
    newCoordsReattempt = locList._destinationLoc
    watchID2 = navigator.geolocation.watchPosition(success3,error, options)
    map.panTo(locList._startLoc);
    var popup1 = new mapboxgl.Popup()// mark the start location onto map
        .setLngLat(locList._startLoc)
        .setHTML('Start')
        .addTo(map)

    var marker1 = new mapboxgl.Marker()
        .setLngLat(locList._startLoc)
        . addTo(map)  

           
    var popup2 = new mapboxgl.Popup()// mark the ending location onto map
        .setLngLat(locList._destinationLoc)
        .setHTML('Ending')
        .addTo(map)

    var marker2 = new mapboxgl.Marker()
        .setLngLat(locList._destinationLoc)
        . addTo(map) 

    localStorage.removeItem("boolean")
}
