// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";



/* Run Class
    //ATTRIBUTES
    
    startLoc = starting location of the user
    destinationLoc = coords of the generated location
    path = path taken by user
    startTime = date when starting the run
    endTime = date after finishing the run
    time = time taken by user to the destination
    runName = run name inputted by user

    //METHODS
    
    getter and setters of all attributes
    
    distanceCalculator()
    {input: none
    {urpose: calculating the distance between two coordinates (lng and lat) from attributes of startLoc and destination Loc and return the distance}
    
    timeCalculator(num)
    {input: num (0 or 1)}
    {purpose: if 0, it start counting the time from stopWatch() function , and if it is 1, it stop counting and save the time to this._time attributes}
    
    initialize(objectPDO) 
    {input: object public data only}
    {purpose: set value into the private attributes from the objectPDO (used when parsing the run Object)}
*/
class Run {

	constructor(startLoc,destinationLoc,path,startTime,endTime,time,runName) {
	this._startLoc = startLoc;
	this._destinationLoc = destinationLoc;
	this._path = path;
	this._startTime = startTime;
	this._endTime = endTime;
    this._time = time;
    this._runName = runName;
	}
    // all getters
	get startLoc () {
		return this._startLoc;
	}
	get destinationLoc () {
		return this._destinationLoc;
	}
    get path () {
		return this._path;
	}
    
	get startTime () {
		return this._startTime 
	}
	get endTime () {
		return this._endTime 
	}
    get time () {
		return this._time 
	}
    get runName () {
		return this._runName 
	}

    //all setters
    set startLoc(x) {
		this._startLoc = x;
	}
    set destinationLoc(x) {
		this._destinationLoc = x;
	}
    set path(x) {
		this._path = x;
	}
    set startTime (x) {
		this._startTime  = x;
	}
    set endTime(x) {
		this._endTime  = x;
	}
    set time(x) {
		this._time  = x;
	}
    set runName(x) {
		this._runName  = x;
	}

    //formula to calculate distance
	distanceCalculator() {
        var distance = Math.sqrt(Math.pow(this._startLoc.lng-this._destinationLoc.lng,2)+Math.pow(this._startLoc.lat-this._destinationLoc.lat,2))*150000
        return distance
    }

    //create timer
    timeCalculator(){
        this._time = totalDurationInSecond;
    }

    //
    initialize(objectPDO) {
		this._startLoc = objectPDO._startLoc
		this._destinationLoc = objectPDO._destinationLoc
		this._path = objectPDO._path
		this._startTime = objectPDO._startTime
		this._endTime = objectPDO._endTime
        this._time = objectPDO._time
        this._runName = objectPDO._runName
	}
}
	
/*
getTimeStamp(d)
Input: d (a Date object)
Purpose: Give the current date (DD-MM-YYYY) and the time 
*/
function getTimeStamp(d){
	var day = d.getUTCDay() + 8;
	var month = d.getUTCMonth() + 1;
	var year = d.getUTCFullYear();
    var hours =  d.getHours();
    var minutes = d.getMinutes();
    var ampm
    if (minutes < 10){
        minutes = '0' + minutes;
    }
    if (hours >= 12){
        ampm = "PM";
    }	
    else{
        ampm = "AM";
    }
    var dayDateStamp = `${day}-${month}-${year} ${hours}:${minutes}${ampm}`;
	return dayDateStamp;
}
//
var List= []; 
// save the run
function saveRun(){
    // pass infomation to Region object
        runObj._runName = prompt('Save as:','');
    
        List.push(runObj); // pass Region object to an array
        // check if there is a runObj 
        if(localStorage.getItem("runObj") === null){ // if not set a brand new runObj in localstorage
            localStorage.setItem("runObj", JSON.stringify(List));
        }else{// update runObj in localStorage
            var reList =JSON.parse(localStorage.getItem("runObj"));
            reList.push(runObj);
            localStorage.setItem("runObj",JSON.stringify(reList)); 
        }
    }



function reAttempt(){

    localStorage.setItem("boolean", 'true');
}
