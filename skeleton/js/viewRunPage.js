//Code for viewRun.html

let current =[145.1329004, -37.9118667]; //Showing the map at the center of Monash University

// Map object
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 18,
    center: current
});

//Delete run Object 
function deleteRun() {
    dataTake.splice(inx,1)
    localStorage.setItem('runObj',JSON.stringify(dataTake))
}

var inx = localStorage.getItem('Index') //Taking index from local storage 
var dataTake = JSON.parse(localStorage.getItem('runObj')) //Take the run Object (with no methods) from the localStorage


var newRunObj = new Run() // Create a new container object for the parsed object
newRunObj.initialize(dataTake[inx]) //Reinitialize  the object with methods

var dateViewRun = newRunObj.startTime //take starting run date
var distanceViewRun = newRunObj.distanceCalculator().toFixed(2) //Calculate distance
var duration = newRunObj.time //Take time
var avgSpeed = Math.round(distanceViewRun*100/duration)/100 //Calculate Average speed


// display saved information (name,distance,date,speed,distance)
document.getElementById("headerBarTitle").innerHTML = newRunObj.runName; //name 
document.getElementById('date').innerHTML += dateViewRun //date
document.getElementById('distanceTravelled').innerHTML += distanceViewRun +' m'; //distance
document.getElementById('avgSpeed').innerHTML += avgSpeed +' m/s';  //average speed 
document.getElementById('duration').innerHTML += duration + ' s';  //duration                                                       

//Map update
 
var currentPosition = newRunObj.startLoc //Take starting location coordinates
var destination = newRunObj.destinationLoc //Take destination coordinates

var marker1 = new mapboxgl.Marker() //marker for start
    .setLngLat(currentPosition)
    . addTo(map)  

var marker2 = new mapboxgl.Marker() //marker for destination
    .setLngLat(destination)
    . addTo(map)      

//Line Path
    map.on('load', function () {
        map.addLayer({
            "id": "route",
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [[currentPosition.lng,currentPosition.lat], [destination.lng,destination.lat]]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#888",
                "line-width": 8
            }
        });
    });
map.panTo(currentPosition) //Pan to the currentposition